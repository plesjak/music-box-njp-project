const mongoose = require('mongoose');

const songSchema = new mongoose.Schema({
    songId: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    thumbnail: {
        type: String,
        required: true
    },
    category: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }],
    downloaded: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        required: true,
        default: Date.now()
    }
});

module.exports = mongoose.model('Song', songSchema);