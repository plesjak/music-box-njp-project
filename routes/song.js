const express = require('express');
const router = express.Router();
const Song = require('../models/song');
const Playlist = require('../models/playlist');
const fs = require('fs');
const ytdl = require('ytdl-core');

const songsPath = './public/assets/audio/';

//GET ALL
router.get('/', (req, res) => {
    Song.find(function(err, songs) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.status(200).json(songs);
    });
});

//CREATE ONE
router.post('/', (req, res) => {
    const song = new Song({
        name: req.body.name
    });

    song.save(function(err) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.sendStatus(201);
    });
});

//GET SINGLE
router.get('/:id', (req, res) => {
    Song.findById(req.params.id, function(err, song) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.status(200).json(song);
    });
});

//UPDATE ONE
router.put('/:id', (req, res) => {

    Song.findById(req.params.id, function(err, song) {

        if (err) {
            res.status(400).json({ error: err.message });
        }

        song.code = req.body.code;
        song.name = req.body.name;

        song.save(function(err) {
            if (err) {
                res.status(400).json({ error: err.message });
            }

            res.sendStatus(202);
        });

    });
});

//DELETE ONE
router.delete('/:id', (req, res) => {

    Song.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.sendStatus(202);
    });
});

//DOWNLOAD
router.post('/download', (req, res) => {

    const id = req.body.song.videoId;
    const playlistId = req.body.playlistId;
    const file = songsPath + id + '.mp3';

    const url = 'http://www.youtube.com/watch?v=' + id;

    let song_id;

    if (song_id = this.songDownloading(id)) {
        console.log('Downloading');
        this.addSongToPlaylist(playlistId, song_id);
        return res.status(200).json({ downloaded: 0 });
    } else if (song_id = this.songDownloaded(id)) {
        console.log('Downloaded');
        this.addSongToPlaylist(playlistId, song_id);
        return res.status(200).json({ downloaded: 1 });
    } else {
        console.log('Searching download data');
        ytdl.getInfo(id, (err, info) => {
            if (err) {
                console.log(err.message);
                res.status(400);
            }

            // itag 249 250 140 251

            const song = new Song({
                songId: req.body.song.videoId,
                title: req.body.song.title,
                description: req.body.song.description,
                thumbnail: req.body.song.thumbnail,
                category: req.body.song.category,
                downloaded: 0
            });

            song.save(function(err) {
                if (err) {
                    console.log(err.message);
                    return res.sendStatus(400);
                }

                console.log('Saved ' + id);
                console.log('Downloading ' + id);
            });

            this.addSongToPlaylist(playlistId, song._id);

            let audioFormats = ytdl.filterFormats(info.formats, 'audioonly');

            audioFormats.sort((a, b) => (a.itag > b.itag) ? -1 : 1); //Od najbolje kvalitete prema najlosijoj
            let currentFormat = 0;

            ytdl(url, { format: audioFormats[currentFormat] })
                .pipe(fs.createWriteStream(file))
                .on('error', function(e) {
                    console.log(e.message);
                })
                .on('finish', function() {
                    Song.findOne({ songId: id }, function(err, song) {
                        if (err) {
                            console.log(err.message);
                        } else if (song) {
                            song.downloaded = 1;
                            song.save();
                        }
                    });
                    console.log('Downloaded ' + id);
                });

            return res.sendStatus(202);
        });
    }
});

this.addSongToPlaylist = async(playlistId, songId) => {
    console.log('Adding song to playlist');
    await Playlist.findOne({ _id: playlistId }, function(err, playlist) {
        if (err) {
            console.log(err.message);
            return;
        }

        if (playlist) {
            let songAlreadyInPlaylist = playlist.songs.filter((song) => {
                return song == songId;
            });
            console.log("songAlreadyInPlaylist " + songAlreadyInPlaylist);
            if (songAlreadyInPlaylist != undefined) {
                playlist.songs.push(songId);
            }
            playlist.save();
        }
        console.log('test1');
    });
    console.log('test2');
}

this.songDownloading = (id) => {
    const file = songsPath + id + '.mp3';
    fs.exists(file, (exists) => {
        if (exists) {
            Song.findOne({ songId: id }, function(err, song) {
                if (err) {
                    console.log(err.message);
                }

                if (!song.downloaded) {
                    return song._id;
                } else {
                    return 0;
                }
            });
        }
    });
}

this.songDownloaded = (id) => {
    const file = songsPath + id + '.mp3';
    fs.exists(file, (exists) => {
        if (exists) {
            Song.findOne({ songId: id }, function(err, song) {
                if (err) {
                    console.log(err.message);
                }

                if (song.downloaded) {
                    return song._id;
                } else {
                    return 0;
                }
            });
        }
    });
}

//DOWNLOAD
router.get('/downloaded/:id', (req, res) => {
    const id = req.params.id;
    Song.findOne({ songId: id }, function(err, song) {
        if (err) {
            return res.status(400);
        } else if (song) {
            return res.status(200).json({ downloaded: song.downloaded });
        }
    });
});

//GET STREAM
router.get('/stream/:id', (req, res) => {
    const id = req.params.id;
    const filePath = songsPath + id + '.mp3';
    let stat = fs.statSync(filePath);

    res.writeHead(200, {
        'Content-Type': 'audio/mpeg',
        'Content-Length': stat.size
    });

    let readStream = fs.createReadStream(filePath);
    readStream.pipe(res);

});

module.exports = router;