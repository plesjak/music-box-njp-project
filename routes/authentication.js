const express = require('express');
const router = express.Router();
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');

router.post('/login', (req, res) => {

    const userNameSearch = req.body.username;
    const passwordSearch = req.body.password;

    User.findOne({ username: userNameSearch }, (err, foundUser) => {
        if (err || !foundUser) {
            return res.sendStatus(400);
        }

        const dbHash = foundUser.password;

        if (bcrypt.compareSync(passwordSearch, dbHash)) {
            const user = { name: foundUser.username };

            const accessToken = generateAccessToken(user);

            console.log(foundUser);

            const loginedUser = {
                _id: foundUser._id,
                username: foundUser.username,
                firstname: foundUser.firstname,
                lastname: foundUser.lastname,
                email: foundUser.email,
                pictureurl: foundUser.pictureurl,
                role: foundUser.role,
                accessToken: accessToken
            };

            res.status(200).json({ user: loginedUser });
        } else {
            res.status(401);
        }

    });

});

router.post('/logout', (req, res) => {
    const userNameSearch = req.body.username;
    User.findOne({ username: userNameSearch }, (err, foundUser) => {
        if (err) {
            return res.sendStatus(400);
        }

        foundUser.refreshtoken = '';
        foundUser.save();

        res.sendStatus(204);
    });

});

router.post('/register', (req, res) => {

    const user = new User({
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password),
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        role: '5e0b1669242da4319cc74806'
    });

    user.save().then(data => {
        res.sendStatus(201);
    }).catch(err => {
        res.status(400).json({ error: err.message });
    });

});

function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '10m' });
}

module.exports = router;