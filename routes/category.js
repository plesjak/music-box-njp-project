const express = require('express');
const router = express.Router();
const Category = require('../models/category');

//GET ALL
router.get('/', (req, res) => {
    Category.find(function(err, categories) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.status(200).json(categories);
    });
});

//CREATE ONE
router.post('/', (req, res) => {
    const category = new Category({
        code: req.body.code,
        name: req.body.name
    });

    category.save(function(err) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.sendStatus(201);
    });
});

//GET SINGLE
router.get('/:id', (req, res) => {
    Category.findById(req.params.id, function(err, category) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.status(200).json(category);
    });
});

//UPDATE ONE
router.put('/:id', (req, res) => {

    Category.findById(req.params.id, function(err, category) {

        if (err) {
            res.status(400).json({ error: err.message });
        }

        category.code = req.body.code;
        category.name = req.body.name;

        category.save(function(err) {
            if (err) {
                res.status(400).json({ error: err.message });
            }

            res.sendStatus(202);
        });

    });
});

//DELETE ONE
router.delete('/:id', (req, res) => {

    Category.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.sendStatus(202);
    });
});

module.exports = router;