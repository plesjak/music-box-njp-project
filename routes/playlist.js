const express = require('express');
const router = express.Router();
const Playlist = require('../models/playlist');

//GET ALL
router.get('/', (req, res) => {
    let shared = req.query.shared;
    let userId = req.query.userId;
    console.log(shared);
    console.log(userId);
    if (shared && shared == 1) {
        Playlist.find({ shared: 1 }).populate('songs').exec((err, playlists) => {
            if (err) {
                return res.status(400).json({ error: err.message });
            }
            console.log('public');

            res.status(200).json(playlists);
        });
    } else {
        Playlist.find({ userId: userId }).populate('songs').exec((err, playlists) => {
            if (err) {
                return res.status(400).json({ error: err.message });
            }
            console.log('private');
            res.status(200).json(playlists);
        });
    }

});

//CREATE ONE
router.post('/', (req, res) => {
    const playlist = new Playlist({
        name: req.body.name,
        shared: req.body.shared,
        userId: req.body.userId
    });

    playlist.save(function(err) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.sendStatus(201);
    });
});

//GET SINGLE
router.get('/:id', (req, res) => {
    Playlist.findById(req.params.id, function(err, playlist) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.status(200).json(playlist);
    });
});

//UPDATE ONE
router.put('/', (req, res) => {

    Playlist.findById(req.body._id, function(err, playlist) {

        if (err) {
            res.status(400).json({ error: err.message });
        }

        playlist.name = req.body.name;
        playlist.shared = req.body.shared;
        playlist.songs = req.body.songs;

        playlist.save(function(err) {
            if (err) {
                res.status(400).json({ error: err.message });
            }

            res.sendStatus(202);
        });

    });
});

//DELETE ONE
router.delete('/:id', (req, res) => {

    Playlist.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.sendStatus(202);
    });
});

module.exports = router;