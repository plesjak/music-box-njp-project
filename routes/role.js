const express = require('express');
const router = express.Router();
const Role = require('../models/role');

//GET ALL
router.get('/roles', (req, res) => {
    Role.find(function(err, roles) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.status(200).json(roles);
    });
});

//CREATE ONE
router.post('/roles', (req, res) => {
    const role = new Role({
        name: req.body.name
    });

    role.save(function(err) {
        if (err) {
            return res.status(400).json({ error: err.message });
        }

        res.sendStatus(201);
    });
});

//GET SINGLE
router.get('/roles/:id', (req, res) => {
    Role.findById(req.params.id, function(err, role) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.status(200).json(role);
    });
});

//UPDATE ONE
router.put('/roles/:id', (req, res) => {

    Role.findById(req.params.id, function(err, role) {

        if (err) {
            res.status(400).json({ error: err.message });
        }

        role.code = req.body.code;
        role.name = req.body.name;

        role.save(function(err) {
            if (err) {
                res.status(400).json({ error: err.message });
            }

            res.sendStatus(202);
        });

    });
});

//DELETE ONE
router.delete('/roles/:id', (req, res) => {

    Role.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(400).json({ error: err.message });
        }

        res.sendStatus(202);
    });
});

module.exports = router;