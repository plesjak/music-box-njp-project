const express = require('express');
const router = express.Router();
const fs = require('fs');
const ytdl = require('ytdl-core');

//GET SINGLE
router.get('/download/:id', (req, res) => {
    const videoId = req.params.id;
    const url = 'http://www.youtube.com/watch?v=' + videoId;
    const file = '../public/audio/' + videoId + '.mp3';

    fs.exists(file, (exists) => {
        if (exists) {
            return res.sendStatus(200);
        }
    });

    ytdl.getInfo(videoId, (err, info) => {
        if (err) {
            return res.sendStatus(400);
        }
        /*
        itag
        249
        250
        140
        251
        */

        const audioFormats = ytdl.filterFormats(info.formats, 'audioonly');
        audioFormats.sort((a, b) => (a.itag > b.itag) ? -1 : 1); //Od najbolje kvalitete prema najlosijoj
        let currentFormat = 0;

        ytdl(url, { format: audioFormats[currentFormat] })
            .pipe(fs.createWriteStream(file))
            .on('error', function(e) {
                console.log(e);
            })
            .on('finish', function() {
                console.log('Downloaded ' + videoId);
            });

        return res.sendStatus(202);

        /*
        const stat = fs.statSync(file);
        const total = stat.size;
        if (req.headers.range) {
    
        }
        fs.exists(file, (exists) => {
            if (exists) {
                const range = req.headers.range;
                const parts = range.replace(/bytes=/, '').split('-');
                const partialStart = parts[0];
                const partialEnd = parts[1];
    
                const start = parseInt(partialStart, 10);
                const end = partialEnd ? parseInt(partialEnd, 10) : total - 1;
                const chunksize = (end - start) + 1;
                const rstream = fs.createReadStream(file, {start: start, end: end});
    
                res.writeHead(206, {
                    'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
                    'Accept-Ranges': 'bytes', 'Content-Length': chunksize,
                    'Content-Type': 'audio/mpeg'
                });
                rstream.pipe(res);
    
            } else {
                res.status(400).send('Error - 404');
                res.end();
            }
        });
        */

    });

});

module.exports = router;