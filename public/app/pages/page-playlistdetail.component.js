musicBox.component('playlistdetail', {

    templateUrl: './app/pages/page-playlistdetail.template.html',
    controller: function($scope, $stateParams, PlaylistService, PlayerService) {
        this.playlistId = $stateParams.playlistId;
        this.playlist = PlaylistService.getPlaylist(this.playlistId);

        $scope.$on('playlistDataFetched', () => {
            this.playlist = PlaylistService.getPlaylist(this.playlistId);
        });

        $scope.$on('playerPlaylistUpdated', () => {
            debugger;
            let updatedPlaylist = PlayerService.getPlaylist(this.playlistId);
            if (updatedPlaylist) {
                this.playlist = updatedPlaylist;
            }
        });

        this.playPlaylist = () => {
            PlayerService.setPlaylist(this.playlist);
        }
    },
    controllerAs: 'c'

});