musicBox.component('register', {

    templateUrl: './app/pages/page-register.template.html',
    controller: function($state, AuthenticationService) {

        this.register = function() {
            let username = this.newUserForm.username.$modelValue,
                password = this.newUserForm.password.$modelValue,
                firstname = this.newUserForm.firstname.$modelValue,
                lastname = this.newUserForm.lastname.$modelValue,
                email = this.newUserForm.email.$modelValue;
            AuthenticationService.register(username, password, firstname, lastname, email);
        }

        this.redirectToLogin = function() {
            $state.go('login');
        }

    },
    controllerAs: 'c'

});