musicBox.component('login', {

    templateUrl: './app/pages/page-login.template.html',
    controller: function($state, AuthenticationService) {

        this.login = function() {
            AuthenticationService.login(this.loginData);
        }

        this.redirectToRegister = function() {
            $state.go('register');
        }


    },
    controllerAs: 'c'

});