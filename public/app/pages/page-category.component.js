musicBox.component('category', {

    templateUrl: './app/pages/page-category.template.html',
    controller: function($scope, YouTubeService) {

        this.songs = [];

        this.getShiftAnimation = function() {
            if (this.songs.length > 0) {
                return 'search-top-shift-not-empty';
            }
        }

        $scope.$on('searchDataFound', () => {
            this.songs = YouTubeService.getFoundData();
        });

    },
    controllerAs: 'c'

});