musicBox.component('audioPlayer', {

    templateUrl: './app/components/audio-player.template.html',
    controller: function($scope, PlayerService) {

        this.playlist = [];
        this.player = document.getElementById('soundBoxAudioPlayer');
        this.currentSongName = document.getElementById('soundBoxAudioPlayerSongName');
        this.currentPlaylistName = document.getElementById('soundBoxAudioPlayerPlaylistName');
        this.currentPlaylistName.innerText = 'No playlist selected';
        this.track = 0;
        this.recusrion = 0;

        this.player.addEventListener('ended', () => {
            this.nextSong();
        });

        this.nextSong = (playing) => {
            this.player.pause();
            this.track += 1;
            if (this.track >= this.playlist.songs.length) {
                this.track = 0;
            }
            if (this.songNotDownloaded()) {
                this.nextSong(true);
            }
            this.currentSongName.innerText = this.playlist.songs[this.track].title;
            this.player.src = this.gerUrl(this.track);
            if (!playing)
                this.player.play();
        }

        this.previousSong = (playing) => {
            this.player.pause();
            this.track -= 1;
            if (this.track <= 0) {
                this.track = this.playlist.songs.length - 1;
            }
            if (this.songNotDownloaded()) {
                this.previousSong(true);
            }
            this.currentSongName.innerText = this.playlist.songs[this.track].title;
            this.player.src = this.gerUrl(this.track);
            this.player.play();
            if (!playing)
                this.player.play();
        }

        this.songNotDownloaded = () => {
            return !this.playlist.songs[this.track].downloaded;
        }

        this.gerUrl = (iterator) => {
            return './api/songs/stream/' + this.playlist.songs[iterator].songId;
        }

        $scope.$on('playerPlaylistChanged', () => {
            this.playlist = PlayerService.getPlaylist();
            this.currentPlaylistName.innerText = this.playlist.name;
            this.track = 0;
            if (this.songNotDownloaded()) {
                this.nextSong();
            }
            this.currentSongName.innerText = this.playlist.songs[this.track].title;
            this.player.src = this.gerUrl(this.track);
            this.player.play();
        });

        $scope.$on('playerPlaylistUpdated', () => {
            this.playlist = PlayerService.getPlaylist();
        });
    },
    controllerAs: 'c'

});