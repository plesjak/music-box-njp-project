musicBox.component('playlistForm', {

    templateUrl: './app/components/playlist-form.template.html',
    controller: function($scope, PlaylistService) {

        $scope.data = {
            shared: [
                { text: 'Private', value: 0 },
                { text: 'Public', value: 1 }
            ],
            selectedShared: { text: 'Private', value: 0 }
        };

        this.createNewPlaylist = function() {

            let playlist = {
                name: this.newPlaylistForm.name.$modelValue,
                shared: this.newPlaylistForm.shared.$modelValue.value
            };

            console.log(playlist);

            PlaylistService.save(playlist);
        }
    },
    controllerAs: 'c'

});