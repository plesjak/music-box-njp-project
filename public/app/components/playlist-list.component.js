musicBox.component('playlistList', {

    templateUrl: './app/components/playlist-list.template.html',
    controller: function($scope, PlaylistService) {

        $scope.$on('playlistDataFetched', () => {
            this.playlists = PlaylistService.getPlaylists();
        });

        this.$onInit = function() {
            this.playlists = PlaylistService.getPlaylists();
        };

    },
    controllerAs: 'c'

});