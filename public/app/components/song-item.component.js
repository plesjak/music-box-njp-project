musicBox.component('songItem', {

    templateUrl: './app/components/song-item.template.html',
    bindings: {
        data: '<',
        controls: '<'
    },
    controller: function($scope, PlaylistService) {

        this.$onInit = function() {
            if (PlaylistService.getPlaylists() != undefined && PlaylistService.getPlaylists().length > 0) {
                $scope.data = {
                    playlists: PlaylistService.getPlaylists(),
                    selectedPlaylist: PlaylistService.getPlaylists()[0]
                };
            } else {
                $scope.data = {
                    playlists: PlaylistService.getPlaylists(),
                    selectedPlaylist: PlaylistService.getPlaylists()[0]
                };
            }
        };

        $scope.$on('playlistDataFetched', () => {
            $scope.data.playlists = PlaylistService.getPlaylists();
            $scope.data.selectedPlaylist = PlaylistService.getPlaylists()[0];
        });

        this.addToPlaylist = function() {
            PlaylistService.addToPlaylist(this.addToPlaylistForm.playlist.$modelValue._id, this.data);
        }

        this.downloadedClass = function() {
            if (this.controls != undefined && this.controls == true) {
                if (this.data.downloaded) {
                    return 'shadow-box-downloaded';
                } else {
                    return 'shadow-box-not-downloaded';
                }
            }
            return 'shadow-box';
        }

        this.playlistsEmpty = function() {
            if ($scope.data.playlists != undefined && $scope.data.playlists.length > 0) {
                return false;
            } else {
                return true;
            }
        }

    },
    controllerAs: 'c'

});