musicBox.component('playlistFilter', {

    templateUrl: './app/components/playlist-filter.template.html',
    controller: function(PlaylistService) {
        this.checked = 0;

        this.toggleClicked = () => {
            this.checked += 1;;
            if (this.checked == 2) {
                this.checked = 0;
            }
            PlaylistService.fetch(this.checked);
        }
    },
    controllerAs: 'c'

});