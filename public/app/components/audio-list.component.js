musicBox.component('audioList', {

    templateUrl: './app/components/audio-list.template.html',
    bindings: {
        playlistsongs: '<'
    },
    controller: function($scope, YouTubeService) {

        $scope.$on('searchDataFound', () => {
            this.songs = YouTubeService.getFoundData();
        });

        this.$onInit = function() {
            if (this.playlistsongs) {
                this.songs = this.playlistsongs;
                this.hideSongActions = true;
            }
        };

    },
    controllerAs: 'c'

});