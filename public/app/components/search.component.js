musicBox.component('search', {

    templateUrl: './app/components/search.template.html',
    bindings: {
        categoryfilter: '<'
    },
    controller: function($scope, CategoryService, YouTubeService) {

        this.$onInit = function() {
            console.log(this.categoryfilter);
            if (this.categoryfilter) {
                if (CategoryService.getCategories() != undefined && CategoryService.getCategories().length > 0) {
                    $scope.data = {
                        categories: CategoryService.getCategories(),
                        selectedCategory: CategoryService.getCategories()[0]
                    };
                } else {
                    $scope.data = {
                        categories: CategoryService.getCategories(),
                        selectedCategory: CategoryService.getCategories()[0]
                    };
                }
            }
        };

        this.searchIsDisabled = function() {
            let q = this.searchForm.searchText.$modelValue;
            if (this.categoryfilter) {
                return false;
            } else {
                if (q)
                    return false
            }
            return true;
        }

        this.search = function() {
            console.log(this.searchForm);
            let q = this.searchForm.searchText.$modelValue;
            if (this.categoryfilter) {
                let topic = this.searchForm.category.$modelValue.code;
                if (q) {
                    YouTubeService.searchByCategoryAndText(q, topic);
                } else {
                    YouTubeService.searchByCategory(topic);
                }
            } else {
                YouTubeService.search(q);
            }

        }
    },
    controllerAs: 'c'

});