musicBox.component('playlistItem', {

    templateUrl: './app/components/playlist-item.template.html',
    bindings: {
        data: '<'
    },
    controller: function($state) {

        this.showPlaylistInfo = function() {
            $state.go('playlist', { playlistId: this.data._id });
        }
    },
    controllerAs: 'c'

});