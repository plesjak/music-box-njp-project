class YouTubeService {

    constructor($http, $rootScope, CategoryService) {
        this.http = $http;
        this.key = 'AIzaSyAbgF9wJkCAiSLbSaJvdBVglDgfLsUdr4E';
        this.rootScope = $rootScope;
        this.categoryService = CategoryService;
        this.searchData = [];
    }

    search = (searchText) => {
        this.searchData = [];
        this.http.get('https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&videoCategoryId=10&maxResults=50&q=' + searchText + '&key=' + this.key).then(replay => {
            const categories = this.categoryService.getCategories();
            replay.data.items.forEach(video => {
                let foundVideoCategories = [];
                this.http.get('https://www.googleapis.com/youtube/v3/videos?part=topicDetails&maxResults=1&videoCategoryId=10&id=' + video.id.videoId + '&key=' + this.key).then(replay => {
                    replay.data.items[0].topicDetails.relevantTopicIds.forEach(topicId => {
                        const category = categories.filter((c) => c.code == topicId)[0];
                        if (category != undefined) {
                            foundVideoCategories.push(category._id);
                        }
                    });

                    let foundVideo = {
                        videoId: video.id.videoId,
                        title: video.snippet.title,
                        description: video.snippet.description,
                        thumbnail: video.snippet.thumbnails.medium.url,
                        category: foundVideoCategories
                    }
                    this.searchData.push(foundVideo);
                });
            });
            console.log(this.searchData);
            this.rootScope.$broadcast('searchDataFound');
        });
    }

    searchByCategory = (searchTopic) => {
        this.searchData = [];
        this.http.get('https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&videoCategoryId=10&maxResults=50&topicId=' + searchTopic + '&key=' + this.key).then(replay => {
            const categories = this.categoryService.getCategories();
            replay.data.items.forEach(video => {
                let foundVideoCategories = [];
                this.http.get('https://www.googleapis.com/youtube/v3/videos?part=topicDetails&maxResults=1&videoCategoryId=10&id=' + video.id.videoId + '&key=' + this.key).then(replay => {
                    replay.data.items[0].topicDetails.relevantTopicIds.forEach(topicId => {
                        const category = categories.filter((c) => c.code == topicId)[0];
                        if (category != undefined) {
                            foundVideoCategories.push(category._id);
                        }
                    });

                    let foundVideo = {
                        videoId: video.id.videoId,
                        title: video.snippet.title,
                        description: video.snippet.description,
                        thumbnail: video.snippet.thumbnails.medium.url,
                        category: foundVideoCategories
                    }
                    this.searchData.push(foundVideo);
                });
            });
            console.log(this.searchData);
            this.rootScope.$broadcast('searchDataFound');
        });
    }

    searchByCategoryAndText = (searchText, searchTopic) => {
        this.searchData = [];
        this.http.get('https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&videoCategoryId=10&maxResults=50&q=' + searchText + '&topicId=' + searchTopic + '&key=' + this.key).then(replay => {
            const categories = this.categoryService.getCategories();
            replay.data.items.forEach(video => {
                let foundVideoCategories = [];
                this.http.get('https://www.googleapis.com/youtube/v3/videos?part=topicDetails&maxResults=1&videoCategoryId=10&id=' + video.id.videoId + '&key=' + this.key).then(replay => {
                    replay.data.items[0].topicDetails.relevantTopicIds.forEach(topicId => {
                        const category = categories.filter((c) => c.code == topicId)[0];
                        if (category != undefined) {
                            foundVideoCategories.push(category._id);
                        }
                    });

                    let foundVideo = {
                        videoId: video.id.videoId,
                        title: video.snippet.title,
                        description: video.snippet.description,
                        thumbnail: video.snippet.thumbnails.medium.url,
                        category: foundVideoCategories
                    }
                    this.searchData.push(foundVideo);
                });
            });
            console.log(this.searchData);
            this.rootScope.$broadcast('searchDataFound');
        });
    }

    getFoundData() {
        return this.searchData;
    }

}

musicBox.service('YouTubeService', YouTubeService);