class PlaylistService {

    constructor($http, $rootScope, PlayerService, AuthenticationService) {
        this.http = $http;
        this.rootScope = $rootScope;
        this.playerService = PlayerService;
        this.authenticationService = AuthenticationService;
        this.data = [];
        this.fetch(0);
    }

    fetch(shared) {
        this.http.get('./api/playlists?shared=' + shared + '&userId=' + this.authenticationService.getUser()._id).then(replay => {
            if (replay.status == 200) {
                this.data = replay.data;
                this.rootScope.$broadcast('playlistDataFetched');
                this.playerService.updateCurrentPlaylist(this.data);
            }
        });
    }

    save(playlist) {
        debugger;
        playlist.userId = this.authenticationService.getUser()._id;
        this.http.post('./api/playlists', playlist).then(replay => {
            if (replay.status == 201) {
                alert('Playlists created!');
            }
            this.fetch();
        });
    }

    addToPlaylist(playlistId, song) {

        this.http.post('./api/songs/download', { song: song, playlistId: playlistId }).then(replay => {
            setTimeout(() => {
                this.fetch();
                if (replay.status == 200) {
                    if (replay.data.downloaded == 0) {
                        //already downloading
                        this.fetchWhenDownloaded(song.videoId);
                    } else if (replay.data.downloaded == 1) {
                        //Downloaded
                    }
                } else if (replay.status == 202) {
                    //Started to downloading

                    this.fetchWhenDownloaded(song.videoId);
                }
            }, 100);
        });
    }

    fetchWhenDownloaded(songId) {
        let updated = false;
        let downloadInterval;
        downloadInterval = setInterval(() => {
            try {
                this.http.get('./api/songs/downloaded/' + songId).then(replay => {
                    if (!updated && replay.data.downloaded == 1) {
                        this.fetch();
                        updated = true;
                        clearInterval(downloadInterval);
                    }
                });
            } catch (error) {}
        }, 10000);
    }

    getPlaylists() {
        return this.data;
    }

    getPlaylist(playlistId) {
        return this.data.filter((x) => x._id = playlistId)[0];
    }

}

musicBox.service('PlaylistService', PlaylistService);