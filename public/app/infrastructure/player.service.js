class PlayerService {

    constructor($http, $rootScope) {
        this.http = $http;
        this.rootScope = $rootScope;
        this.playlistData = undefined;
    }

    setPlaylist(playlist) {
        this.playlistData = playlist;
        this.rootScope.$broadcast('playerPlaylistChanged');
    }

    updateCurrentPlaylist(playlists) {
        debugger;
        if(this.playlistData) {
            this.playlistData = playlists.filter((x) => x._id == this.playlistData._id);
        }
        this.rootScope.$broadcast('playerPlaylistUpdated');
    }

    getPlaylist() {
        return this.playlistData;
    }

}

musicBox.service('PlayerService', PlayerService);