musicBox.component('sideBar', {
    template: `
    <nav class="navbar-dark bg-dark sidebar">
        <ul class="nav flex-column text-center">
            <li class="nav-item">
            <a class="nav-link side-nav-btn" ng-class="c.getClass('main')" ui-sref="main">Search</a>
            </li>
            <li class="nav-item">
            <a class="nav-link side-nav-btn" ng-class="c.getClass('playlists')" ui-sref="playlists">Playlists</a>
            </li>
            <li class="nav-item">
            <a class="nav-link side-nav-btn" ng-class="c.getClass('categories')" ui-sref="categories">Categories</a>
            </li>
            <li class="nav-item">
            <a class="nav-link side-nav-btn" ng-class="c.getClass('users')" ui-sref="favorites">Users</a>
            </li>
        </ul>
    </nav>
    `,
    controller: function($state) {

        this.getClass = function(c) {
            if ($state.current.name == c) return 'side-nav-btn-active';

        }

    },
    controllerAs: 'c'
});