musicBox.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('main', {
        url: '/',
        component: 'main'
    }).state('playlists', {
        url: '/playlists',
        component: 'playlist'
    }).state('playlist', {
        url: '/playlist/:playlistId',
        component: 'playlistdetail'
    }).state('categories', {
        url: '/categories',
        component: 'category'
    }).state('favorites', {
        url: '/favorites',
        component: 'favorite'
    }).state('login', {
        url: '/login',
        component: 'login'
    }).state('register', {
        url: '/register',
        component: 'register'
    });

    $urlRouterProvider.otherwise('/');


});