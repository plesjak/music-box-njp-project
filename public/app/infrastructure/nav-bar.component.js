musicBox.component('navBar', {
    template: `
    <nav class="navbar navbar-expand-lg navbar-dark bg-middle sticky-top">
        <a class="navbar-brand" ui-sref="main">
            Music Box
        </a>

        <div class="mr-auto">
        </div>
        <img src="./assets/img/user/default.jpg" height="42" width="42" class="rounded-circle" alt="{{c.user.firstname}} {{c.user.lastname}}">
        <span class="navbar-text ml-2 mr-4">
            {{c.user.firstname}} {{c.user.lastname}}
        </span>
        <button ng-click="c.logout()" class="btn btn-outline-danger my-2 my-sm-0" type="submit">Log out</button>
    </nav>
    `,
    controller: function($scope, AuthenticationService) {

        let admin = 0;

        $scope.$on('userChangedState', () => {
            this.user = AuthenticationService.getUser();
        });
    },
    controllerAs: 'c'
});