class AuthenticationService {

    constructor($http, $state, $rootScope) {
        this.http = $http;
        this.user = null;
        this.state = $state;
        this.rootScope = $rootScope;
    }

    isAuthenticated() {
        let auth = false;

        if (this.user != null || sessionStorage.getItem('authenticated') == "true") {
            auth = true;
        }

        if (auth) {
            this.user = JSON.parse(sessionStorage.getItem('user'));
        }

        console.log(auth);

        return auth;
    }

    getToken() {
        return this.user.accessToken;
    }

    getUser() {
        return this.user;
    }

    login(loginData) {
        this.http.post('./auth/login', loginData).then(replay => {
            if (replay.status == 200) {
                this.user = replay.data.user;
                sessionStorage.setItem('authenticated', true);
                sessionStorage.setItem('user', JSON.stringify(replay.data.user));
                this.state.go('main');
                this.rootScope.$broadcast('userChangedState');
            } else {
                alert('Wrong credentials');
            }
        });
    }

    logout() {
        this.http.post('./auth/logout', { username: this.user.username }).then(replay => {
            sessionStorage.removeItem('authenticated');
            sessionStorage.removeItem('user');
            this.user = null;
            this.rootScope.$broadcast('userChangedState');
            this.state.go('login');
        });
    }

    register(username, password, firstname, lastname, email) {
        this.http.post('./auth/register', { username, password, firstname, lastname, email }).then(replay => {
            console.log(replay);
            if (replay.status == 201) {
                alert('Registration successful!');
                this.state.go('login');
            } else {
                alert('Something went wrong...\n\rPlease try again later');
            }
        });
    }
}

musicBox.service('AuthenticationService', AuthenticationService);