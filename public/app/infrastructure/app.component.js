musicBox.component('app', {

    template: `
        <nav-bar ng-show="c.authenticated"></nav-bar>
        <div class="container-fluid p-0">
            <side-bar ng-show="c.authenticated"></side-bar>
            <div class="content-container" ui-view></div>
        </div>
        <div class="audio-player" ng-show="c.authenticated">
            <audio-player></audio-player>
        </div>
        
        `,
    controller: function ($scope, $state, AuthenticationService) {

        let authenticated = AuthenticationService.isAuthenticated();
        if (!authenticated) {
            $state.go('login');
        }

        $scope.$on('userChangedState', () => {
            this.authenticated = AuthenticationService.isAuthenticated();
        });

    },
    controllerAs: 'c'
});
