class CategoryService {

    constructor($http, $rootScope) {
        this.http = $http;
        this.rootScope = $rootScope;
        this.data = [];
        this.fetch();
    }

    fetch() {
        this.http.get('./api/categories').then(replay => {
            if (replay.status = 200) {
                this.data = replay.data;
                this.rootScope.$broadcast('categoryDataFetched');
            }
        });
    }

    getCategories() {
        return this.data;
    }

}

musicBox.service('CategoryService', CategoryService);