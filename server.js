require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');

const rootFolder = __dirname + '/public';

app.use(express.static(rootFolder));

//Parse body data to JSON
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


//DataBase
mongoose.connect(process.env.DB_CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to Database'));


//Import routes
const authRoute = require('./routes/authentication');
app.use('/auth', authRoute);

//Midleware
const categoryRoute = require('./routes/category');
const playlistRoute = require('./routes/playlist');
const songRoute = require('./routes/song');

/*
app.use('/api/songs', midleware);
app.use('/api/categories', midleware);
app.use('/api/playlists', midleware);
*/

app.use('/api/songs', songRoute);
app.use('/api/categories', categoryRoute);
app.use('/api/playlists', playlistRoute);



/*
const playlistRoute = require('./routes/playlist');
app.use('/playlist', playlistRoute);
*/

//Routes
app.get('*', function(req, res) {
    res.sendFile(path.join(rootFolder + '/app/index.html'));
});

const port = process.env.PORT || 8081;
app.listen(port, () => console.log('Running on port ' + port));


function midleware(req, res, next) {
    var token = req.body.token || req.params.token || req.headers['x-access-token'];
    console.log('req.body');
    console.log(req.body);
    console.log('req.params');
    console.log(req.params);
    console.log('req');
    console.log(req);
    if (token) {
        jwt.verify(token, secret, function(err, decoded) {
            if (err) {
                return res.status(403).send({
                    success: false,
                    message: 'Wrong token'
                });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token'
        });
    }
}